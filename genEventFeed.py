### Fetch from DOMjudge v4 REST API
### This is an essential implementation based on:
###   1. PC^2 9.6.0
###   2. https://clics.ecs.baylor.edu/index.php/Event_Feed

import requests
import json
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom

from datetime import datetime


URL = 'https://www.domjudge.org/demoweb/api/v4/contests/nwerc18'
AUTH = ('jury', 'jury')

CONTEST_TITLE = 'PROGRAMMING CONTEST'


region_map = {}
judgement_map = {}
problem_map = {}


def str2datetime(isoform):
    return datetime.strptime(isoform.split('+')[0], '%Y-%m-%dT%H:%M:%S.%f')


def seconds2str(sec):
    return '{:02d}:{:02d}:{:02d}'.format(sec//3600, (sec%3600)//60, sec%60)


def build_info(root):
    j = json.loads(requests.get(URL+'/state', auth=AUTH).text)

    started = str2datetime(j['started'])
    ended = str2datetime(j['ended'])
    frozen = str2datetime(j['frozen'])

    handle = ET.Element('info')

    c = ET.SubElement(handle, 'length')
    c.text = seconds2str((ended-started).seconds)

    c = ET.SubElement(handle, 'penalty')
    c.text = str(20)

    c = ET.SubElement(handle, 'starttime')
    c.text = str(started.timestamp())

    c = ET.SubElement(handle, 'title')
    c.text = CONTEST_TITLE

    c = ET.SubElement(handle, 'scoreboard-frozen-length')
    c.text = seconds2str((ended-frozen).seconds)

    root.append(handle)


def build_judgement(root):
    j = json.loads(requests.get(URL+'/judgement-types', auth=AUTH).text)

    for it in j:
        handle = ET.Element('judgement')
        c = ET.SubElement(handle, 'acronym')
        c.text = it['id']
        c = ET.SubElement(handle, 'name')
        c.text = it['name']
        judgement_map[it['id']] = str(it['penalty'])
        root.append(handle)


def build_language(root):
    j = json.loads(requests.get(URL+'/languages', auth=AUTH).text)

    for it in j:
        handle = ET.Element('language')
        c = ET.SubElement(handle, 'name')
        c.text = it['name']
        root.append(handle)


def build_problem(root):
    j = json.loads(requests.get(URL+'/problems', auth=AUTH).text)

    for it in j:
        handle = ET.Element('problem')
        c = ET.SubElement(handle, 'id')
        c.text = problem_map[it['id']] = str(it['ordinal']+1)
        c = ET.SubElement(handle, 'name')
        c.text = it['name']
        c = ET.SubElement(handle, 'letter')
        c.text = it['label']
        c = ET.SubElement(handle, 'test_data_count')
        c.text = str(it['test_data_count'])
        root.append(handle)


def build_region(root):
    j = json.loads(requests.get(URL+'/groups', auth=AUTH).text)

    for it in j:
        handle = ET.Element('region')
        c = ET.SubElement(handle, 'id')
        c.text = it['id']
        c = ET.SubElement(handle, 'name')
        c.text = region_map[it['id']] = it['name']
        root.append(handle)


def build_team(root):
    if not region_map:
        build_region(root)

    j = json.loads(requests.get(URL+'/teams', auth=AUTH).text)

    for it in j:
        handle = ET.Element('team')
        c = ET.SubElement(handle, 'external-id')
        c.text = it['externalid']
        c = ET.SubElement(handle, 'id')
        c.text = it['id']
        c = ET.SubElement(handle, 'name')
        c.text = it['name']
        c = ET.SubElement(handle, 'nationality')
        c.text = it['nationality']
        c = ET.SubElement(handle, 'region')
        c.text = region_map[it['group_ids'][0]]
        c = ET.SubElement(handle, 'university')
        c.text = it['affiliation']
        root.append(handle)


def build_run(root):
    if not judgement_map:
        build_judgement(root)

    if not problem_map:
        build_problem(root)


if __name__ == '__main__':
    root = ET.Element('contest')

    build_info(root)
    build_judgement(root)
    build_language(root)
    build_problem(root)
    build_team(root)
    build_run(root)

    event_feed_xml = ET.tostring(root).decode('utf-8')
    event_feed_xml = minidom.parseString(event_feed_xml).toprettyxml(encoding='utf-8').decode('utf-8')
    print(event_feed_xml)

